# Grok

Clojure-based Q&A site

## Templating

I use Hiccup and Garden to generate HTML and CSS respectively,
which allows me to intertwine state data and templates, having
both not only in the same text but also in the same language.

See `str/grok{styles,pages`.

Also, when necessary, external data is easy to parse into Clojure
via readers. Have a look at the data file `resources/data/example.edn`
(in edn, this is, Clojure) and how it specifies a reader (a function)
that will be used to transform the data it reads.

For instance

```clojure
{:questions
 [{:id 1
   :user-id 1
   :title "Is Clojure that awesome?"
   :body #md #file "data/question1.md"
   ...
   }]}
```

We are telling the compiler to use a function that is specified
somewhere in our code. In this case, read the string "data/question1.md"
and use the reader #file which will read the contents of the file
with that name. This is, in turn, read by #md, which parses that string
as Markdown content and returns a formatted HTML string. If we needed to
use logic and state, we would use Clojure, but in this case it is only
text and Markdown is superior for this task.

## Routes

HTTP routes are defined in `str/clj/grok/routes/home.clj`

```clojure
(def home-routes
  ["/" {:get  {""           home-page
               "about"      about-page
               ["question/" :question-id]   question-page
               ;"lacinia"    get-lacinia
               ["css/" :style] get-css
               ["test/" :p] get-test}
        :post {["test/" :p] post-test}}])
```

Important are questions and css. You can follow from there how CSS is
generated on the fly for each request (a simple memoize -cache- is needed
in production). This allows to modify the source code and see it on every
request.

## Type classes

Perhaps you are interested in having a look at commit

`9c9da702528db98942f45894d467f14c3024791d Replace styles ns reflection with multimethod`


## Beware

A lot of libraries have been included to eventually play with
and the resulting package is heavy.

## Next steps

In dependency order

- Loging pages and authentication (buddy library).
- Server/Client rendering. Share the same page source code between
the server (Clojure) and the Client (Clojurescript/Reagent)
- User interaction (forms)
- Scaling/optimization considerations
- Cred API integration
- Database considerations for off-network data
- Thinning the JAR & (optional) use Graal to build zero-overhead native binary

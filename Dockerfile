FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/grok.jar /grok/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/grok/app.jar"]

(ns grok.db
  (:require
    [clojure.java.io :as io]
    [clojure.edn :as edn]
    [markdown-to-hiccup.core :as m]))

(def readers {'md   (fn [t]
                      (->> t
                           (m/md->hiccup)
                           (m/component)))
              'file (fn [n] (slurp (io/resource n)))})

(defn load-all []
  (edn/read-string
    {:readers readers}
    (slurp (io/resource "data/example.edn"))))

(defn get-questions []
  (:questions (load-all)))

(defn get-question-by-id [id]
  (first (filter (comp (partial = id) :id)
                 (get-questions))))

(defn get-users []
  (:users (load-all)))

(defn get-user-by-id [id]
  (first (filter (comp (partial = id) :id)
                 (get-users))))
(ns grok.schema
  (:require
    [com.walmartlabs.lacinia.schema :as lacinia-schema]
    [com.walmartlabs.lacinia.util :refer [attach-resolvers]]
    [clojure.java.io :as io]
    [clojure.tools.reader.edn :as edn]))

(defn get-hero [context arguments value]
  (let [{:keys [episode]} arguments]
    (if (= episode :NEWHOPE)
      {:id 1000
       :name "Luke"
       :home_planet "Tatooine"
       :appears_in ["NEWHOPE" "EMPIRE" "JEDI"]}
      {:id 2000
       :name "Lando Calrissian"
       :home_planet "Socorro"
       :appears_in ["EMPIRE" "JEDI"]})))

#_(def schema
  (-> (io/resource "schema.edn")
      slurp
      edn/read-string
      (attach-resolvers {:get-hero get-hero
                         :get-droid (constantly {})})
      lacinia-schema/compile))



(ns grok.styles
  (:require
    [garden.core :as garden]))

(defmulti get-style
          (fn [style]
            (->> style name (format "grok.styles.%s/style") keyword)))

(defn css [style]
  (garden/css (into [] (get-style style))))
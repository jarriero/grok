(ns grok.pages.about
  (:require
    [grok.pages.base :as base]))

(defn content []
  [:img {:src "/img/warning_clojure.png"}])

(defn page []
  (base/page (content)))

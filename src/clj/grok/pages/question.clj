(ns grok.pages.question
  (:require
    [grok.db :as db]
    [grok.pages.base :as base]))

(defn content [question]
  [:div
   [:div.col_main
    [:div.question
     [:h1 (:title question)]
     [:div (:body question)]
     (let [u (db/get-user-by-id (:user-id question))]
       [:div.by "by " [:a {:href ""} (:username u)]])
     [:div.score (str (:upvotes question) " points")]]
    [:div
     [:p (for [answer (:answers question)]
           [:div.answer-container
            [:div.answer-body
             [:div (:body answer)]
             (let [u (db/get-user-by-id (:user-id answer))]
               [:div.by "by " [:a {:href ""} (:username u)]])]
            [:div (for [comment (:comments answer)]
                    [:div.comment
                     [:div.comment-body (:body comment)]
                     (let [u (db/get-user-by-id (:user-id comment))]
                       [:div.by "by " [:a {:href ""} (:username u)]])])]])]]]
   [:div.col_right
    [:h1 "Related questions"]
    [:div (for [q (db/get-questions)
                :when (not= (:id q) (:id question))]
            [:div [:a {:href (str "/question/" (:id q))} (:title q)]])]]])

(defn page [question]
  (base/page (content question)))

(ns grok.pages.home
  (:require
    [grok.pages.base :as base]))

(defn content [questions]
  [:div
   (for [q questions]
     [:div [:a {:href (str "/question/" (:id q))} (:title q)]])])

(defn page [question]
  (base/page (content question)))

(ns grok.pages.base)

(defn- head []
  [:head
   [:meta {:http-equiv "Content-Type" :content "text/html; charset=UTF-8"}]
   [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
   [:title "Welcome to grok"]
   [:link {:href "/assets/bootstrap/css/bootstrap.min.css"
           :rel "stylesheet" :type "text/css"}]
   [:link {:href "/assets/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css"
           :rel "stylesheet" :type "text/css"}]
   [:link {:href "/css/base.css"
           :rel "stylesheet" :type "text/css"}]
   [:link {:href "/css/question.css"
           :rel "stylesheet" :type "text/css"}]])

(defn nav []
  [:nav.navbar.navbar-dark.bg-primary.navbar-expand-md {:role "navigation"}
   [:button.navbar-toggler.hidden-sm-up
    {:type        "button"
     :data-toggle "collapse"
     :data-target "#collapsing-navbar"}
    [:span.navbar-toggler-icon]]
   [:a.navbar-brand {:href "/"} "grok"]
   [:div.collapse.navbar-collapse {:id "collapsing-navbar"}
    [:ul.nav.navbar-nav
     [:li.nav-item
      [:a.nav-link {:href "/"} "home"]]
     [:li.nav-item
      [:a.nav-link {:href "/about"} "about"]]]]])

(defn- body [content]
  [:body
   (nav)
   [:div.content content]])

(defn page [content]
  [:html
   (head)
   (body content)])

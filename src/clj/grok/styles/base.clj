(ns grok.styles.base
  (:require
    [grok.styles :refer [get-style]]))

(defmethod grok.styles/get-style ::style [_]
  {:body {:word-wrap "breakout"}
   :.content {:margin "0 auto"
              :width "1002px"}
   :.grid-page {}
   :.col_right {:width "314px"
                :float "left"
                :min-height "1px"
                :margin-left "40px"
                :font-size "13px"}
   :.col_main {:width "602px"
               :float "left"
               :margin-left "30px"
               :min-height "1px"}})


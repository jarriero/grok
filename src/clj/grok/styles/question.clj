(ns grok.styles.question
  (:require
    [grok.styles :refer [get-style]]))


(defmethod grok.styles/get-style ::style [_]
  {:.answer-container {:padding "16px 0"
                       :border-top "1px solid #e2e2e2"
                       :position "relative"
                       :margin-bottom "0px"}
   :.answer-header {:font-size "14px"}
   :.answer-body {:margin-bottom "60px"}
   :.question {:margin-bottom "50px"}
   :.by {:margin-bottom "10px" :float "left"}
   :.score {:float "right"}
   :.answer-content {}
   :.comment-body {:font-size "14px"}
   :.comment {:margin-bottom "60px" :margin-top "25px" :margin-left "30px"}})
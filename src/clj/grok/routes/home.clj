(ns grok.routes.home
  (:require
    [grok.styles.base]
    [grok.styles.question]
    [grok.pages.about :as about]
    [grok.pages.home :as page-home]
    [grok.pages.question :as page-question]
    [grok.styles :as styles]
    [grok.schema :as schema]
    [grok.db :as db]
    [com.walmartlabs.lacinia :refer [execute]]
    [ring.util.response :as res]
    [ring.util.http-response :as http]
    [hiccup.core :as h]))

(defn locate-question [question-id]
  (first (filter (comp (partial = question-id) :id) (db/get-questions))))

(defn render [page]
  (h/html page))

(defn home-page [& _]
  (http/content-type
    (http/ok
      (render (page-home/page (db/get-questions))))
    "text/html; charset=utf-8"))

(defn question-page [{:keys [route-params]}]
  (http/content-type
    (http/ok
      (render (page-question/page
                (locate-question (Long/parseLong (:question-id route-params))))))
    "text/html; charset=utf-8"))

(defn about-page [& _]
  (http/content-type
    (http/ok
      (render (about/page)))
    "text/html; charset=utf-8"))

#_(defn get-lacinia [{:keys [query-params]}]
  (let [query (get query-params "query")]
    (http/ok
      (execute schema/schema query nil nil))))

(defn get-test [{:keys [route-params]}]
  (let [{:keys [p]} route-params]
    (res/response
      (http/ok (str "Param get is " p)))))

(defn post-test [{:keys [route-params]}]
  (let [{:keys [p]} route-params]
    (res/response
      (http/ok (str "Param post is " p)))))

(defn get-css [{:keys [route-params]}]
  (let [file-name (get route-params :style)
        [_ ns*] (re-matches #"^(.*)\..*" file-name)]
    (http/content-type
      (http/ok (styles/css ns*))
      "text/css")))

(def home-routes
  ["/" {:get  {""           home-page
               "about"      about-page
               ["question/" :question-id]   question-page
               ;"lacinia"    get-lacinia
               ["css/" :style] get-css
               ["test/" :p] get-test}
        :post {["test/" :p] post-test}}])


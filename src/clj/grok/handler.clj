(ns grok.handler
  (:require [grok.layout :refer [error-page]]
            [grok.routes.home :refer [home-routes]]
            [grok.middleware :as middleware]
            [grok.env :refer [defaults]]
            [bidi.ring :refer [make-handler]]
            [mount.core :as mount]))

(mount/defstate init-app
  :start ((or (:init defaults) identity))
  :stop  ((or (:stop defaults) identity)))

(mount/defstate app
  :start
  (middleware/wrap-base
    (-> (make-handler home-routes)
        middleware/wrap-csrf
        middleware/wrap-formats)))

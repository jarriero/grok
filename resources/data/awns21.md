The reasons I would definitely use Clojure 
for this specific tasks are the following:

- A ready-to-use it-just-works approach. Handling data like symbols 
 in code makes prototyping much faster without losing maintainability.
 External data can be easily handled with EDN readers, such as this
 very markup, from the data model structures (have a look at
 `example.edn` and the accompanying Markdown files under `resources`
 directory).
- The lack of artificial (to the problem) constructs makes Clojure
 a very practical language, allowing the programmer
 to see what is being built as statically as possible. The mind is
 free from translating the data into language objects. Operations on
 data are reflected in the most declarative way, so the programmer
 can almost see the result of the program because data dominates
 the most area of the screen as opposed to language instructions in
 other programming languages.
- Reagent is a very easy-to-work-with implementation of React, allowing
 the backend programmer to write client side applications (SPAs)
 almost as if he was a front-end developer (i.e., leverage your
 skills, no need for another developer and another front end language)
- Clojure/Clojurescript isomorphism allows for easy server-side
 pre-rendering of your application, allowing you to choose the degree
 of SPA vs static content of your application (e.g., from almost
 full static content Grok to entirely SPA file storage service),
 so that SEO is available for your specifically selected content.
- This level of flexibility allows to build a semi-automatic site
 generation template according to the level of client-side rendering
 vs SEO desired.
- I can personally bring ideas on scalability derived from my work
 for an established major publisher with millions of visits per day.
- Since the apps are non core, they can be rewritten and sites migrated
to any other language if it is proved that the new language
outperforms Clojure in any required measure. Or replaced just for the
sake of it. 

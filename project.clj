(defproject grok "0.1.0-SNAPSHOT"

  :description "Q&A system"
  :url "https://bitbucket.org/jarriero/grok"

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [bidi "2.1.3"]
                 [floatingpointio/graphql-builder "0.1.6"]
                 [hiccup "1.0.5"]
                 [markdown-to-hiccup "0.3.0"]

                 ; Back-end-only stuff
                 [org.clojure/tools.cli "0.3.6"]
                 [org.clojure/tools.logging "0.4.0"]
                 [ch.qos.logback/logback-classic "1.2.3"]
                 [org.slf4j/log4j-over-slf4j "1.7.25"]
                 [org.slf4j/jul-to-slf4j "1.7.25"]
                 [org.slf4j/jcl-over-slf4j "1.7.25"]
                 [aero "1.1.3"]
                 [integrant "0.6.3"]
                 [buddy "2.0.0"]
                 [clj-time "0.14.3"]
                 [com.walmartlabs/lacinia "0.26.0"]
                 [com.netflix.hystrix/hystrix-clj "1.5.12"]
                 [conman "0.7.8"]
                 [cprop "0.1.11"]
                 [funcool/struct "1.2.0"]
                 [luminus-immutant "0.2.4"]
                 [luminus-migrations "0.5.0"]
                 [luminus-nrepl "0.1.4"]
                 [luminus/ring-ttl-session "0.3.2"]
                 [markdown-clj "1.0.2"]
                 [metosin/muuntaja "0.5.0"]
                 [metosin/ring-http-response "0.9.0"]
                 [mount "0.1.12"]
                 [org.webjars.bower/tether "1.4.3"]
                 [org.webjars/bootstrap "4.1.0"]
                 [org.webjars/font-awesome "5.0.10"]
                 [org.webjars/jquery "3.2.1"]
                 [ring-webjars "0.2.0"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-defaults "0.3.1"]
                 [selmer "1.11.7"]
                 [com.h2database/h2 "1.4.196"]

                 ; Front-end-only stuff
                 [org.clojure/clojurescript "1.9.908"]
                 [reagent "0.7.0"]
                 [re-frame "0.10.5"]
                 [com.andrewmcveigh/cljs-time "0.5.0"]
                 [re-com "2.1.0"]
                 [garden "1.3.2"]
                 ]

  :min-lein-version "2.5.3"

  :source-paths ["src/clj"]
  :test-paths ["test/clj"]
  :resource-paths ["resources"]
  :target-path "target/%s/"
  :main ^:skip-aot grok.core

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"
                                    "test/js"
                                    "resources/public/css"]

  :garden {:builds [{:id           "screen"
                     :source-paths ["src/clj"]
                     :stylesheet   grok.css/screen
                     :compiler     {:output-to     "resources/public/css/screen.css"
                                    :pretty-print? true}}]}

  :less {:source-paths ["less"]
         :target-path  "resources/public/css"}

  :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}

  :plugins [[lein-immutant "2.1.0"]
            [lein-cljsbuild "1.1.5"]
            [lein-garden "0.2.8"]
            [lein-less "1.7.5"]]

  :cljsbuild {:builds
              [{:id           "dev"
                :source-paths ["src/cljs"]
                :figwheel     {:on-jsload "x.core/mount-root"}
                :compiler     {:main                 x.core
                               :output-to            "resources/public/js/compiled/app.js"
                               :output-dir           "resources/public/js/compiled/out"
                               :asset-path           "js/compiled/out"
                               :source-map-timestamp true
                               :preloads             [devtools.preload
                                                      day8.re-frame-10x.preload]
                               :closure-defines      {"re_frame.trace.trace_enabled_QMARK_" true
                                                      "day8.re_frame.tracing.trace_enabled_QMARK_" true}
                               :external-config      {:devtools/config {:features-to-install :all}}
                               }}

               {:id           "min"
                :source-paths ["src/cljs"]
                :compiler     {:main            x.core
                               :output-to       "resources/public/js/compiled/app.js"
                               :optimizations   :advanced
                               :closure-defines {goog.DEBUG false}
                               :pretty-print    false}}

               {:id           "test"
                :source-paths ["src/cljs" "test/cljs"]
                :compiler     {:main          x.runner
                               :output-to     "resources/public/js/compiled/test.js"
                               :output-dir    "resources/public/js/compiled/test/out"
                               :optimizations :none}}
               ]}

  :profiles {:uberjar {:omit-source true
                       :aot :all
                       :uberjar-name "grok.jar"
                       :source-paths ["env/prod/clj"]
                       :resource-paths ["env/prod/resources"]}

             :prod         {:dependencies [[day8.re-frame/tracing-stubs "0.5.0"]]}
             :dev           [:project/dev :profiles/dev]
             :test          [:project/dev :project/test :profiles/test]

             :project/dev  {:jvm-opts ["-Dconf=dev-config.edn" "--add-modules" "java.xml.bind"]
                            :dependencies [[expound "0.5.0"]
                                           [pjstadig/humane-test-output "0.8.3"]
                                           [prone "1.5.2"]
                                           [ring/ring-devel "1.6.3"]
                                           [ring/ring-mock "0.3.2"]]
                            :plugins      [[com.jakemccrary/lein-test-refresh "0.19.0"]]

                            :source-paths ["env/dev/clj"]
                            :resource-paths ["env/dev/resources"]
                            :repl-options {:init-ns user}
                            :injections [(require 'pjstadig.humane-test-output)
                                         (pjstadig.humane-test-output/activate!)]}
             :project/test {:jvm-opts ["-Dconf=test-config.edn" "--add-modules" "java.xml.bind"]
                            :resource-paths ["env/test/resources"]}
             :profiles/dev {:dependencies [[org.clojure/tools.nrepl "0.2.12"]
                                           [integrant/repl "0.3.1"]
                                           [figwheel-sidecar "0.5.13"]
                                           [com.cemerick/piggieback "0.2.2"]
                                           [binaryage/devtools "0.9.4"]
                                           [day8.re-frame/re-frame-10x "0.3.0"]
                                           [day8.re-frame/tracing "0.5.0"]]

                            :plugins      [[lein-figwheel "0.5.13"]
                                           [lein-doo "0.1.8"]
                                           [lein-pdo "0.1.1"]]}
             :profiles/test {}}

  :aliases {"dev" ["do" "clean"
                   ["pdo" ["figwheel" "dev"]
                    ["less" "auto"]
                    ["garden" "auto"]]]
            "build" ["with-profile" "+prod,-dev" "do"
                     ["clean"]
                     ["cljsbuild" "once" "min"]
                     ["less" "once"]
                     ["garden" "once"]]})

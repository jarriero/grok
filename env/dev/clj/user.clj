(ns user
  (:require [grok.config :refer [env]]
            [grok.core :refer [start-app]]
            [grok.db.core :as db]
            [clojure.spec.alpha :as s]
            [ns-tracker.core :as ns-tracker]
            [expound.alpha :as expound]
            [mount.core :as mount]
            [conman.core :as conman]
            [luminus-migrations.core :as migrations]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

#_(def modified-namespaces
  (ns-tracker/ns-tracker ["src/clj" "env/dev/clj" "test/clj"]))

(defn start []
  (mount/start-without #'grok.core/repl-server))

(defn stop []
  (mount/stop-except #'grok.core/repl-server))

(defn restart []
  (stop)
  (start))

(defn restart-db []
  (mount/stop #'grok.db.core/*db*)
  (mount/start #'grok.db.core/*db*)
  (binding [*ns* 'grok.db.core]
    (conman/bind-connection grok.db.core/*db* "sql/queries.sql")))

(defn reset-db []
  (migrations/migrate ["reset"] (select-keys env [:database-url])))

(defn migrate []
  (migrations/migrate ["migrate"] (select-keys env [:database-url])))

(defn rollback []
  (migrations/migrate ["rollback"] (select-keys env [:database-url])))

(defn create-migration [name]
  (migrations/create name (select-keys env [:database-url])))



(ns grok.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [grok.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[grok started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[grok has shut down successfully]=-"))
   :middleware wrap-dev})
